# Time-dependent detector response

This is a Python script to compute the time-dependnent response of ground-based gravitational-wave detectors to compact binary signals for the TaylorF2 waveform.

This implements the method in Chen and Johnson-McDaniel, [arXiv:2407.15732](https://arxiv.org/abs/2407.15732), which should be cited in any work that uses this code.

Authors: Anson Chen, Nathan K Johnson-McDaniel (c) 2024
