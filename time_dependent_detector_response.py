# A Python script to compute the time-dependnent response of ground-based gravitational-wave detectors to compact binary signals for the TaylorF2 waveform.
# This implements the method in Chen and Johnson-McDaniel, arXiv:2407.15732 (henceforth the paper), which should be cited in any work that uses this code
# Authors: Anson Chen, Nathan K Johnson-McDaniel (c) 2024

import numpy as np
from pycbc.detector import Detector
import scipy.constants
from lal import MTSUN_SI, PC_SI, DAYSID_SI, YRSID_SI, GAMMA
import astropy.units as u
from astropy.coordinates import get_body_barycentric, get_body_barycentric_posvel, SkyCoord, EarthLocation
from astropy.time import Time

# Uncomment the following lines to use the DE432s JPL ephemeris, which is accurate for 1950 and 2050, but slower than the built-in version
# Replace 'de432s' with 'de430' to use the larger DE430 ephemeris valid between approximately 1550 and 2650, or with the appropriate string
# to use a different ephemeris available from https://naif.jpl.nasa.gov/pub/naif/generic_kernels/spk/planets/
"""
from astropy.coordinates import solar_system_ephemeris
solar_system_ephemeris.set('de432s')
"""

# Values

omegaEarth = 2*np.pi / DAYSID_SI
f_Earth = 1. / DAYSID_SI

TEM_s = 27.32166155*86400 # Moon's mean orbital motion from https://hpiers.obspm.fr/eop-pc/models/constants.html

omegaEM = 2.*np.pi/TEM_s
omegaEMB = 2.*np.pi/YRSID_SI

# TaylorF2 waveform and its derivatives. The waveform amplitude is the leading Newtonian expression, while the phase includes the point particle terms through 3.5PN from Buonanno et al, Phys. Rev. D 80 084043, 2009; the leading spin-orbit effects at 1.5PN and spin-spin effects at 2PN from Kidder, Phys. Rev. D 52, 821, 1995 (both just for spins aligned with the orbital angular momentum); and the tidal deformability effects at 5PN and 6PN from Vines, Flanagan, and Hinderer, Phys. Rev. D 83, 084051, 2011
def Taylor_F2_deri(m1, m2, f, distance, chi1, chi2, lambda1, lambda2, z, t_c=0, phi_c=0):
    '''
    Input parameters:
    m1, m2:                component masses (solar masses)
    f:                     frequency (Hz), should be a numpy array or a single value
    distance:              luminosity distance (Mpc)
    chi1, chi2:            aligned components of dimensionless spins of m1, m2
    lambda1, lambda2:      dimensionless tidal deformabilities of m1, m2
    z:                     redshift
    t_c:                   constant time shift, default is 0
    phi_c:                 constant phase shift default is 0

    Output:                the waveform amplitude, the phase, the first derivative of the phase w.r.t. f
    '''
    m1_s = m1 * MTSUN_SI * (1+z)            # in sec
    m2_s = m2 * MTSUN_SI * (1+z)            # in sec
    eta = m1_s*m2_s/(m1_s+m2_s)**2
    M = eta**(3./5)*(m1_s+m2_s)
    D = distance*1.e6*PC_SI / scipy.constants.c / (1+z)          # in sec
    A = M**(5./6)/D * np.pi**(-2./3) * np.sqrt(5*eta/24)
    v = (np.pi * (m1_s+m2_s) * f)**(1./3)

    # compute spin-orbit and spin-spin parameters
    delta = (m1-m2)/(m1+m2)
    chi_s = 0.5*(chi1+chi2)
    chi_a = 0.5*(chi1-chi2)
    beta = (113./12-19./3*eta)*chi_s + 113./12*delta*chi_a
    sigma = eta * ( 721./48*(chi_s**2-chi_a**2) - 247./48*(chi_s**2-chi_a**2) ) + (1-2*eta)*(719./96*(chi_s**2+chi_a**2) - 233./96*(chi_s**2+chi_a**2)) + delta*(719./48*chi_s*chi_a-233./48*chi_s*chi_a)

    # PN coefficients
    a0 = 1
    a1 = 0
    a2 = 20./9 * (743./336 + 11./4 * eta)
    a3 = -4 *(4 * np.pi - beta)
    a4 = 10 * (3058673./1016064 + 5429./1008 * eta + 617./144 * eta**2) - 10*sigma
    a5 = np.pi * (38645. / 756 - 65. / 9 * eta ) * (1 + 3 * np.log(v))
    a6 = (11583231236531. / 4694215680 - 640 * np.pi**2 / 3 - 6848. / 21 * GAMMA) + eta * (-15737765635. / 3048192 + 2255 * np.pi**2 / 12) + eta**2 * 76055. / 1728 - 127825. / 1296 * eta**3 - 6848. / 21 * np.log(4 * v)
    a7 = np.pi * (77096675./254016 + 378515./1512*eta - 74045./756*eta**2)

    sum_v = a0 + a1*v + a2*v**2 + a3*v**3 + a4*v**4 + a5*v**5 + a6*v**6 + a7*v**7

    # compute the phase for tidal deformability
    al = 8./13*(1+7*eta-31*eta**2)
    bl = 8./13*np.sqrt(1-4*eta)*(1+9*eta-11*eta**2)
    cl = 1./2*np.sqrt(1-4*eta)*(1-13272./1319*eta+8944./1319*eta**2)
    dl = 1./2*(1-15910./1319*eta+32850./1319*eta**2+3380./1319*eta**3)
    el = al+bl
    fl = al-bl
    gl = cl+dl
    hl = cl-dl
    lambda_tilde = el*lambda1 + fl*lambda2
    lambda_delta = gl*lambda1 + hl*lambda2

    psi_tidal = 3./(128*eta*v**5) * ((-39./2*lambda_tilde)*v**10 + (-3115./64*lambda_tilde+6595./364*np.sqrt(1-4*eta)*lambda_delta)*v**12)

    # the phase
    psi_f = 2*np.pi*f*t_c + phi_c - np.pi/4 + 3./(128*eta*v**5)*sum_v + psi_tidal

    # first phase derivative
    dv_df = 1./3*(np.pi * (m1_s+m2_s))**(1./3) * f**(-2./3)
    da5_df = np.pi*(38645./756-65./9*eta) / f
    da6_df = -6848./63/f

    dsumv_df = a1*dv_df + a2*2*v*dv_df + a3*3*v**2*dv_df + a4*4*v**3*dv_df + da5_df*v**5 + a5*5*v**4*dv_df + da6_df*v**6 + a6*6*v**5*dv_df + a7*7*v**6*dv_df

    dpsi_tidal_df = - 15./(128*eta*v**6) * dv_df * ((-39./2*lambda_tilde)*v**10 + (-3115./64*lambda_tilde+6595./364*np.sqrt(1-4*eta)*lambda_delta)*v**12) + 3./(128*eta*v**5) * ((-39./2*lambda_tilde)*10*v**9 + (-3115./64*lambda_tilde+6595./364*np.sqrt(1-4*eta)*lambda_delta)*12*v**11) * dv_df

    dpsif_df = 2 * np.pi * t_c - 15./(128*eta*v**6) * dv_df * sum_v + 3./(128 * eta * v**5) * dsumv_df + dpsi_tidal_df

    return A, psi_f, dpsif_df

# Time-dependent detector response in the frequency domain for TaylorF2
def time_dependent_response_FD_TaylorF2(det_abv, m1, m2, f, distance, chi1, chi2, lambda1, lambda2, inc, ra, dec, pol, gps_time, z, t_c=0, phi_c=0, doppler_shift=False, use_2nd_order_orb=False):
    '''
    Input parameters:
    det_abv:               abbreviation of detector, can take 'H1', 'L1', 'V1', 'K1', 'G1', 'E1', 'E2', or 'E3' when including Doppler shift; any detector known to PyCBC if not
    m1, m2:                component masses (solar masses)
    f:                     frequency (Hz), should be a numpy array or a single value
    distance:              luminosity distance (Mpc)
    chi1, chi2:            aligned components of dimensionless spins of m1, m2
    lambda1, lambda2:      dimensionless tidal deformabilities of m1, m2
    inc:                   inclination angle (radians)
    ra:                    right ascension (radians)
    dec:                   declination (radians)
    pol:                   polarization angle (radians)
    gps_time:              GPS time (s)
    z:                     redshift
    t_c:                   constant time shift, default is 0
    phi_c:                 constant phase shift, default is 0
    doppler_shift:         turn on Doppler shift effect, default is False (off)
    use_2nd_order_orb:     use 2nd order expressions for the orbital motion, default is False (off)
    
    Output:                detector response in the frequency domain
    '''

    # Since astropy doesn't have the ET location, we use V1 for ET, since E1, E2, and E3 in LALSuite/PyCBC are at the V1 location.
    if det_abv=='E1' or det_abv=='E2' or det_abv=='E3':
        det_abv_ap = 'V1'
    else:
        det_abv_ap = det_abv
        
    det = Detector(det_abv)

    m1_s = m1 * MTSUN_SI * (1+z)            # in sec
    m2_s = m2 * MTSUN_SI * (1+z)            # in sec
    eta = m1_s*m2_s/(m1_s+m2_s)**2
    M = eta**(3./5)*(m1_s+m2_s)
    cos_inc = np.cos(inc)
    
    end_time = np.array([gps_time, gps_time+DAYSID_SI/8., gps_time+DAYSID_SI/4., gps_time+DAYSID_SI/2., gps_time+3*DAYSID_SI/4.])
    F_det_td = np.zeros((len(end_time)))

    f_shift = [0, -f_Earth, f_Earth, -2*f_Earth, 2*f_Earth]

    F_h_FFT = [0]*2
    h_FFT = [0]*len(f_shift)

    # compute Doppler shift effects due to Earth's rotation and orbital motion
    if doppler_shift:

        # Compute the time of the middle of the signal in the leading approximation 
        v = (np.pi * (m1_s+m2_s) * f)**(1./3)
        time = 5.*(m1_s+m2_s)/(256*eta*v[0]**8) - 5.*(m1_s+m2_s)/(256*eta*v[-1]**8)

        t_half = time/2
        t_mid_gps = gps_time - t_half

        # detector's location
        det_loc = EarthLocation.of_site(det_abv_ap)

        # Keep the timespan over which the orbital coefficients are determined from being too small
        zetaEM = max(1., 0.2*DAYSID_SI/t_half)
        zetaEMB2 = max(1., 0.5*DAYSID_SI/t_half)

        # Function to compute the projection onto the direction to the source and convert to seconds
        coord = SkyCoord(ra=ra, dec=dec, unit='rad')
        dir_source = coord.cartesian

        def projn(x):
            xdn = x.dot(dir_source)
            return float(xdn.to(u.m)/u.m)/scipy.constants.c

        # Earth's rotational motion
        xdn0 = projn(det_loc.get_gcrs_posvel(Time(t_mid_gps, format='gps'))[0])
        xdn1 = projn(det_loc.get_gcrs_posvel(Time(t_mid_gps + 0.25*DAYSID_SI, format='gps'))[0]) - xdn0
        xdn2 = projn(det_loc.get_gcrs_posvel(Time(t_mid_gps + 0.5*DAYSID_SI, format='gps'))[0]) - xdn0

        b1c = -0.5*xdn2
        b1s = xdn1 - 0.5*xdn2

        # Earth's orbital motion around the Earth-Moon barycentre

        t_mid_conv = Time(t_mid_gps, format='gps')
        xEM = get_body_barycentric('earth', t_mid_conv)
        xEMB = get_body_barycentric('earth-moon-barycenter', t_mid_conv)
        
        xdnEM0 = projn(xEM - xEMB)

        def projn_EMBtoE_shifted(t_gps):
            tconv = Time(t_gps, format='gps') 
            return projn(get_body_barycentric('earth', tconv) - get_body_barycentric('earth-moon-barycenter', tconv)) - xdnEM0

        zEMth = zetaEM*t_half

        oEMzEMth = omegaEM*zEMth

        xdnEM1 = projn_EMBtoE_shifted(t_mid_gps + zEMth)
        xdnEMm1 = projn_EMBtoE_shifted(t_mid_gps - zEMth)

        if not use_2nd_order_orb:
            bEM1c = -(xdnEM1 + xdnEMm1)/(2. - 2.*np.cos(oEMzEMth))
            bEM1s = (xdnEM1 - xdnEMm1)/(2.*np.sin(oEMzEMth))
        else:
            zEMth2 = 0.5*zEMth

            xdnEM05 = projn_EMBtoE_shifted(t_mid_gps + zEMth2)
            xdnEMm05 = projn_EMBtoE_shifted(t_mid_gps - zEMth2)

            oEMzEMth4 = 0.25*oEMzEMth
            oEMzEMth2 = 0.5*oEMzEMth

            cEM4 = np.cos(oEMzEMth4)
            sEM4 = np.sin(oEMzEMth4)

            cEM2 = np.cos(oEMzEMth2)
            sEM2 = np.sin(oEMzEMth2)

            cEM = np.cos(oEMzEMth)
            sEM = np.sin(oEMzEMth)

            s2EM = np.sin(2.*oEMzEMth)

            num_bit_EM1 = -2.*xdnEM05 + xdnEM1 - 2.*xdnEMm05 + xdnEMm1
            num_bit_EM2 = 2.*(xdnEM05 + xdnEMm05)
            num_bit_EM3 = xdnEM1 - xdnEMm1
            num_bit_EM4 = xdnEM05 - xdnEMm05
            den_EM = 16.*sEM4*sEM4*sEM4*sEM4*(1. + 2.*cEM2)

            bEM1c2 = -(num_bit_EM1 - num_bit_EM2*cEM)/den_EM
            bEM1s2 = (num_bit_EM3 - 2.*num_bit_EM4*cEM)*sEM/(2.*(sEM*sEM - sEM2*s2EM))
            bEM2c2 = (num_bit_EM1 - num_bit_EM2*cEM2)/(4.*cEM4*cEM4*den_EM)
            bEM2s2 = (num_bit_EM4/sEM2 - num_bit_EM3/sEM)/(sEM4*sEM4*(8. + 16.*cEM2))

        # The orbital motion of the Earth-Moon barycentre around the solar system barycentre

        xdnEMB0 = projn(xEMB)

        def projn_SSBtoEMB_shifted(t_gps):
            tconv = Time(t_gps, format='gps') 
            return projn(get_body_barycentric('earth-moon-barycenter', tconv)) - xdnEMB0

        if not use_2nd_order_orb:
            oEMBth = omegaEMB*t_half

            xdnEMB1 = projn_SSBtoEMB_shifted(gps_time)
            xdnEMBm1 = projn_SSBtoEMB_shifted(gps_time - time)

            bEMB1c = -(xdnEMB1 + xdnEMBm1)/(2. - 2.*np.cos(oEMBth))
            bEMB1s = (xdnEMB1 - xdnEMBm1)/(2.*np.sin(oEMBth))
        else:
            zEMB2th = zetaEMB2*t_half
            zEMB2th2 = 0.5*zEMB2th

            xdnEMB1s = projn_SSBtoEMB_shifted(t_mid_gps + zEMB2th)
            xdnEMBm1s = projn_SSBtoEMB_shifted(t_mid_gps - zEMB2th)
            xdnEMB05 = projn_SSBtoEMB_shifted(t_mid_gps + zEMB2th2)
            xdnEMBm05 = projn_SSBtoEMB_shifted(t_mid_gps - zEMB2th2)

            oEMBzEMB2th = omegaEMB*zEMB2th
            oEMBzEMB2th2 = 0.5*oEMBzEMB2th
            oEMBzEMB2th4 = 0.25*oEMBzEMB2th

            cEMB4 = np.cos(oEMBzEMB2th4)
            sEMB4 = np.sin(oEMBzEMB2th4)

            cEMB2 = np.cos(oEMBzEMB2th2)
            sEMB2 = np.sin(oEMBzEMB2th2)

            cEMB = np.cos(oEMBzEMB2th)
            sEMB = np.sin(oEMBzEMB2th)

            s2EMB = np.sin(2.*oEMBzEMB2th)

            num_bit_EMB1 = -2.*xdnEMB05 + xdnEMB1s - 2.*xdnEMBm05 + xdnEMBm1s
            num_bit_EMB2 = 2.*(xdnEMB05 + xdnEMBm05)
            num_bit_EMB3 = xdnEMB1s - xdnEMBm1s
            num_bit_EMB4 = xdnEMB05 - xdnEMBm05
            den_EMB = 16.*sEMB4*sEMB4*sEMB4*sEMB4*(1. + 2.*cEMB2)

            bEMB1c2 = -(num_bit_EMB1 - num_bit_EMB2*cEMB)/den_EMB
            bEMB1s2 = (num_bit_EMB3 - 2.*num_bit_EMB4*cEMB)*sEMB/(2.*(sEMB*sEMB - sEMB2*s2EMB))
            bEMB2c2 = (num_bit_EMB1 - num_bit_EMB2*cEMB2)/(4.*cEMB4*cEMB4*den_EMB)
            bEMB2s2 = (num_bit_EMB4/sEMB2 - num_bit_EMB3/sEMB)/(sEMB4*sEMB4*(8. + 16.*cEMB2))

        for k in range(0, len(f_shift)):

            f_use = f + f_shift[k]

            A, psi_f, dpsi_df = Taylor_F2_deri(m1, m2, f_use, distance, chi1, chi2, lambda1, lambda2, z, t_c, phi_c)

            t_SPA = dpsi_df/(2*np.pi) + t_half

            oEtSPA = omegaEarth*t_SPA
            oEth = omegaEarth*t_half

            b_rot = b1c*(np.cos(oEtSPA) - np.cos(oEth)) + b1s*(np.sin(oEtSPA) - np.sin(oEth))
            # The sign between the sine functions is minus, because in this code t_half = -t_mid in the paper.

            oEMtSPA = omegaEM*t_SPA
            oEMth = omegaEM*t_half

            oEMBtSPA = omegaEMB*t_SPA
            oEMBth = omegaEMB*t_half

            if not use_2nd_order_orb:
                b_EM = bEM1c*(np.cos(oEMtSPA) - np.cos(oEMth)) + bEM1s*(np.sin(oEMtSPA) - np.sin(oEMth))
                b_EMB = bEMB1c*(np.cos(oEMBtSPA) - np.cos(oEMBth)) + bEMB1s*(np.sin(oEMBtSPA) - np.sin(oEMBth))
            else:
                oEMtSPAt = 2.*oEMtSPA
                oEMtht = 2.*oEMth

                oEMBtSPAt = 2.*oEMBtSPA
                oEMBtht = 2.*oEMBth

                b_EM = bEM1c2*(np.cos(oEMtSPA) - np.cos(omegaEM*t_half)) + bEM1s2*(np.sin(omegaEM*t_SPA) - np.sin(omegaEM*t_half)) + \
                       bEM2c2*(np.cos(oEMtSPAt) - np.cos(oEMtht)) + bEM2s2*(np.sin(oEMtSPAt) - np.sin(oEMtht))
                b_EMB = bEMB1c2*(np.cos(oEMBtSPA) - np.cos(oEMBth)) + bEMB1s2*(np.sin(oEMBtSPA) - np.sin(oEMBth)) + \
                        bEMB2c2*(np.cos(oEMBtSPAt) - np.cos(oEMBtht)) + bEMB2s2*(np.sin(oEMBtSPAt) - np.sin(oEMBtht))

            # eq. (20) in the paper
            h_FFT[k] = A * f_use**(-7./6) * np.exp((psi_f + 2.*np.pi*f_use*(b_EMB + b_EM + b_rot))*1.j)
    else:
        for k in range(0, len(f_shift)):
            A, psi_f, dpsi_df = Taylor_F2_deri(m1, m2, f+f_shift[k], distance, chi1, chi2, lambda1, lambda2, z)

            h_FFT[k] = A * (f+f_shift[k])**(-7./6) * np.exp(psi_f*1.j)

    # Fourier series method for the time-dependent antenna pattern functions in the frequency domain
    for n in range(0,2):
        for i in range(0,len(end_time)):
            F_det_td[i] = det.antenna_pattern(ra, dec, pol, end_time[i])[n]

        R1 = F_det_td[0]
        R2 = F_det_td[1]
        R3 = F_det_td[2]
        R4 = F_det_td[3]
        R5 = F_det_td[4]

        # eqs. (2) in the paper
        a_0 = (R1+R3+R4+R5)/4
        a_1c = (R1-R4)/2.
        a_1s = (R3-R5)/2.
        a_2c = (R1+R4-R3-R5)/4
        a_2s = R2+((np.sqrt(2)-1)*(R4+R5)-(np.sqrt(2)+1)*(R1+R3))/4

        # eq. (5) in the paper
        F_h_FFT[n] = a_0*h_FFT[0] + 1./2*a_1c*(h_FFT[1]+h_FFT[2]) - 1./2.j*a_1s*(h_FFT[1]-h_FFT[2]) + 1./2*a_2c*(h_FFT[3]+h_FFT[4]) - 1./2.j*a_2s*(h_FFT[3]-h_FFT[4])

    return -(1+cos_inc**2)*F_h_FFT[0] + 2.j*cos_inc*F_h_FFT[1]            # return the sum of detector response of plus and cross polarization
